/**
 * 
 */
package edu.utexas.tacc.xras.jackson;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author mrhanlon
 *
 */
public class Mixin {
  
  @JsonIgnoreProperties({"status", "tags"})
  public static class PublicationMixin extends Mixin {}
  
  @JsonIgnoreProperties("fields")
  public static class PublicationTypeMixin extends Mixin {}
  
}
