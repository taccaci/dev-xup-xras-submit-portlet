/**
 * 
 */
package edu.utexas.tacc.xras.api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.utexas.tacc.xras.ServiceException;
import edu.utexas.tacc.xras.api.exception.AuthException;
import edu.utexas.tacc.xras.http.session.XrasSession;
import edu.utexas.tacc.xras.model.type.AttributeSetRelationType;
import edu.utexas.tacc.xras.model.type.DocumentType;
import edu.utexas.tacc.xras.model.type.FieldOfScienceType;
import edu.utexas.tacc.xras.model.type.RoleType;
import edu.utexas.tacc.xras.model.type.UnitType;
import edu.utexas.tacc.xras.service.type.AttributeSetRelationTypeService;
import edu.utexas.tacc.xras.service.type.DocumentTypeService;
import edu.utexas.tacc.xras.service.type.FieldOfScienceTypeService;
import edu.utexas.tacc.xras.service.type.RoleTypeService;
import edu.utexas.tacc.xras.service.type.UnitTypeService;

/**
 * @author mrhanlon
 *
 */
@Controller
@RequestMapping(value="/xras/types", method=RequestMethod.GET)
public class TypesApi extends BaseApiController {

  @Autowired
  private AttributeSetRelationTypeService attrSetRelService;

  @Autowired
  private UnitTypeService unitService;

  @Autowired
  private DocumentTypeService docTypeService;
  
  @Autowired
  private RoleTypeService roleService;
  
  @Autowired
  private FieldOfScienceTypeService fosService;

  @RequestMapping(value="/units")
  public @ResponseBody List<UnitType> units(HttpServletRequest request) throws AuthException, ServiceException {
    XrasSession session = getXrasSession(request);
    return unitService.list(session);
  }

  @RequestMapping(value="/attribute_set_relations")
  public @ResponseBody Map<Integer, AttributeSetRelationType> attributeSetRelations(HttpServletRequest request) throws AuthException, ServiceException {
    XrasSession session = getXrasSession(request);
    List<AttributeSetRelationType> list = attrSetRelService.list(session);
    Map<Integer, AttributeSetRelationType> map = new HashMap<Integer, AttributeSetRelationType>();
    for (AttributeSetRelationType type : list) {
      map.put(type.getId(), type);
    }
    return map;
  }

  @RequestMapping(value="/documents")
  public @ResponseBody List<DocumentType> documents(HttpServletRequest request) throws AuthException, ServiceException {
    XrasSession session = getXrasSession(request);
    return docTypeService.list(session);
  }

  @RequestMapping(value="/roles")
  public @ResponseBody List<RoleType> roles(HttpServletRequest request) throws AuthException, ServiceException {
    XrasSession session = getXrasSession(request);
    return roleService.list(session);
  }

  @RequestMapping("/fos")
  public @ResponseBody List<FieldOfScienceType> fos(HttpServletRequest request) throws AuthException, ServiceException {
    XrasSession session = getXrasSession(request);
    return fosService.list(session);
  }

  @RequestMapping(value="/fos", params="map=true")
  public @ResponseBody Map<Integer, FieldOfScienceType> fosMap(HttpServletRequest request) throws AuthException, ServiceException {
    XrasSession session = getXrasSession(request);

    List<FieldOfScienceType> fosList = fosService.list(session);
    Map<Integer, FieldOfScienceType> fosMap = new HashMap<Integer, FieldOfScienceType>();
    for (FieldOfScienceType fos : fosList) {
      fosMap.put(fos.getId(), fos);
    }
    return fosMap;
  }
}
