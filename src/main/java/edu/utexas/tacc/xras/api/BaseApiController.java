/**
 * 
 */
package edu.utexas.tacc.xras.api;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonMappingException.Reference;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.google.common.collect.Iterables;
import com.liferay.portal.model.User;
import com.liferay.portal.util.PortalUtil;

import edu.utexas.tacc.xras.ServiceException;
import edu.utexas.tacc.xras.api.exception.AuthException;
import edu.utexas.tacc.xras.http.session.XrasSession;

/**
 * @author mrhanlon
 *
 */
public abstract class BaseApiController {

  protected XrasSession getXrasSession(HttpServletRequest request) throws AuthException {
    User user = null;
    try {
      user = PortalUtil.getUser(request);
    } catch (Exception e) {
      Logger.getLogger(getClass()).error(e.getMessage());
    }
    if (user != null) {
      String sessionUsername, impersonate = (String) request.getSession().getAttribute("impersonateUser");
      if (impersonate != null) {
        sessionUsername = impersonate;
      } else {
        sessionUsername = user.getScreenName();
      }
      
      return new XrasSession(sessionUsername);
    } else {
      throw new AuthException();
    }
  }
  
  @ExceptionHandler(AuthException.class)
  @ResponseBody
  @ResponseStatus(value= HttpStatus.UNAUTHORIZED)
  public String handleAuthException(AuthException e) {
    Logger.getLogger(getClass()).error(e.getMessage(), e);
    return "Authentication required";
  }
  
  @ExceptionHandler({HttpMessageNotReadableException.class, InvalidFormatException.class, JsonMappingException.class})
  @ResponseBody
  @ResponseStatus(value= HttpStatus.BAD_REQUEST)
  public String handleAuthException(Exception e) {
    Logger.getLogger(getClass()).error(e.getMessage(), e);
    
    String message = null;
    Throwable cause = e;
    while (message == null && cause != null) {
      if (cause instanceof JsonMappingException) {
        JsonMappingException ex = ((JsonMappingException) cause);

        Logger.getLogger(getClass()).error(ex.getMessage(), ex);
        
        StringBuilder messageBuilder = new StringBuilder();
        messageBuilder.append("Invalid input for field: ");
        Reference last = Iterables.getLast(ex.getPath());
        messageBuilder.append(last.getFrom().getClass().getSimpleName()).append("->").append(last.getFieldName()).append(".");
        
        if (cause instanceof InvalidFormatException) {
          messageBuilder.append(" The value \"")
            .append(((InvalidFormatException) cause).getValue())
            .append("\" is invalid.");
        }
        message = messageBuilder.toString();
      } else {
        cause = cause.getCause();
      }
    }
    
    if (message != null) {
      return message;
    } else {
      return e.getMessage();
    }
  }

  @ExceptionHandler(ServiceException.class)
  public ResponseEntity<String> handleApiException(HttpServletResponse response, ServiceException e) {
    int statusCode = e.getStatusCode();
    if (statusCode == 0) {
      Throwable t = e;
      while (t.getCause() instanceof ServiceException) {
        statusCode = ((ServiceException) t.getCause()).getStatusCode();
        t = t.getCause();
      }
      if (statusCode == 0) {
        statusCode = 500;
      }
    }
    Logger.getLogger(getClass()).error(e.getMessage(), e);
    return new ResponseEntity<String>(e.getMessage(), HttpStatus.valueOf(statusCode));
  }
  
  /**
   * Attempt to figure out something about what happened.
   * @param e
   * @return
   */
  @ExceptionHandler(Exception.class)
  @ResponseBody
  @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
  public String handleException(Exception e) {
    Throwable cause = e;
    boolean identified = false;
    while (!identified && cause != null) {
      if (cause.getClass().equals(JsonMappingException.class)) {
        identified = true;
        break;
      }
      cause = cause.getCause();
    }
    if (identified) {
      return cause.getMessage();
    } else {
      return e.getMessage();
    }
  }
}
