/**
 * 
 */
package edu.utexas.tacc.xras.api;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.hibernate3.Hibernate3Module;
import com.fasterxml.jackson.datatype.joda.JodaModule;

import edu.utexas.tacc.hibernate.bean.Publication;
import edu.utexas.tacc.hibernate.bean.PublicationType;
import edu.utexas.tacc.xras.jackson.Mixin.PublicationMixin;
import edu.utexas.tacc.xras.jackson.Mixin.PublicationTypeMixin;

/**
 * @author mrhanlon
 * 
 */
@Configuration
public class ApiConfiguration {

  @Bean
  public ObjectMapper objectMapper() {
    ObjectMapper mapper = new ObjectMapper();
    mapper.registerModule(new Hibernate3Module());
    mapper.registerModule(new JodaModule());
    mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
    mapper.addMixInAnnotations(Publication.class, PublicationMixin.class);
    mapper.addMixInAnnotations(PublicationType.class, PublicationTypeMixin.class);
    
    return mapper;
  }
  
  @Autowired
  private RequestMappingHandlerAdapter requestMappingHandlerAdapter;
  
  @PostConstruct
  public void init() {
    // disable caching
    requestMappingHandlerAdapter.setCacheSeconds(0);
    
    List<HttpMessageConverter<?>> messageConverters = requestMappingHandlerAdapter.getMessageConverters();
    for (HttpMessageConverter<?> messageConverter : messageConverters) {
        if (messageConverter instanceof MappingJackson2HttpMessageConverter) {
          ((MappingJackson2HttpMessageConverter) messageConverter).setObjectMapper(objectMapper());
        }
    }
  }
  
  @Bean
  public MultipartResolver multipartResolver() {
    return new CommonsMultipartResolver();
  }
  
}
