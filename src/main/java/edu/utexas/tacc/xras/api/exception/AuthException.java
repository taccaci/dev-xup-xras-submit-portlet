/**
 * 
 */
package edu.utexas.tacc.xras.api.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author mrhanlon
 *
 */
@ResponseStatus(value=HttpStatus.FORBIDDEN)
public class AuthException extends XrasException {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
}
