/**
 * 
 */
package edu.utexas.tacc.xras.api;

import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import edu.utexas.tacc.dao.PortalPersonDao;
import edu.utexas.tacc.xras.service.AbstractPortletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.utexas.tacc.xras.ServiceException;
import edu.utexas.tacc.xras.api.exception.AuthException;
import edu.utexas.tacc.xras.http.session.XrasSession;
import edu.utexas.tacc.xras.model.Person;
import edu.utexas.tacc.xras.service.PersonService;

/**
 * @author mrhanlon
 * 
 */
@Controller
@RequestMapping(value = "/xras/people")
public class PeopleApi extends BaseApiController {

  @Autowired
  private PersonService personService;

  @Autowired
  private AbstractPortletService portletService;

  @RequestMapping(value = "/{username}", method = RequestMethod.GET)
  public
  @ResponseBody
  Person get(HttpServletRequest request, @PathVariable String username) throws AuthException, ServiceException {
      XrasSession session = getXrasSession(request);
      DataSource ds = portletService.getDataSource();
      PortalPersonDao ppdao = new PortalPersonDao(ds);
      if (ppdao.getId(username) > 0) {
          return personService.read(session, new Person(username));
      } else {
          // they're unvetted
          // @TODO this is working but we need to do something with the empty user
          return new Person();
      }
  }

  @RequestMapping(value = "/{username}", method = RequestMethod.POST)
  public
  @ResponseBody
  Person post(HttpServletRequest request, @RequestBody Person person) throws AuthException, ServiceException {
      XrasSession session = getXrasSession(request);
      DataSource ds = portletService.getDataSource();
      PortalPersonDao ppdao = new PortalPersonDao(ds);
      if (ppdao.getId(person.getUsername()) > 0) {
          return personService.save(session, person);
      } else {
          return new Person();
      }
  }

}