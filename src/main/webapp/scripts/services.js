'use strict';

angular.module('xrasApp.service')
  .factory('Notify', function($rootScope, $timeout, AppUtil, _) {
    var messages = [],
      service = {};

    service.messages = function() {
      return messages;
    };

    service.clear = function(key) {
      key = key || 'all';

      if (key === 'all') {
        messages.length = 0;
      } else {
        messages = _.reject(messages, function(m) {
          return m.key === key;
        });
      }
      $rootScope.$broadcast('xras:notify');
    };

    service.remove = function(message) {
      messages = _.reject(messages, function(m) {
        return m._id === message._id;
      });
      $rootScope.$broadcast('xras:notify');
    };

    service.message = function(message, duration) {
      message = _.extend({key: 'app', type: 'info'}, message);
      message.icon = message.icon || (message.type === 'info' ? 'info-sign' : message.type === 'success' ? 'thumbs-up' : 'warning-sign');
      message._id = AppUtil.uuid();
      messages.push(message);

      // automatically dismiss notification after duration milliseconds.
      // setting duration to a number <= 0 makes it sticky.
      // error messages default to sticky. other types default to 5000ms.
      duration = duration || (message.type === 'danger' ? 0 : 5000);
      if (duration > 0) {
        $timeout(function() {
          service.remove(message);
        }, duration);
      }

      $rootScope.$broadcast('xras:notify');
    };

    return service;
  })
  .factory('XrasRestangular', function($cacheFactory, Restangular, Liferay) {
    var cache, baseUrl;

    cache = $cacheFactory('xrasApi');
    baseUrl = '/delegate/xras';

    Restangular.setBaseUrl(baseUrl);
    Restangular.setDefaultHttpFields({cache: cache});

    if (Liferay && Liferay.ThemeDisplay.getDoAsUserIdEncoded()) {
      // handle Liferay impersonation
      Restangular.setDefaultRequestParams({doAsUserId: Liferay.ThemeDisplay.getDoAsUserIdEncoded()});
    }

    Restangular.addResponseInterceptor(function(data, operation) {
      if (operation === 'put' || operation === 'post' || operation === 'delete' || operation === 'remove') {
        cache.removeAll();
      }
      return data;
    });

    return Restangular;
  })
  .factory('Api', function($cacheFactory, XrasRestangular, moment, _) {
    return {
      Opportunities: XrasRestangular.withConfig(function(config) {
        config.setRestangularFields({'id': 'opportunityId'});

        config.addElementTransformer('opportunities', false, function(opportunity) {

          // ensure values exist
          opportunity.rules = opportunity.rules || {};
          opportunity.rules.gracePeriod = opportunity.rules.gracePeriod || 0;

          // normalize name
          opportunity.name = (opportunity.displayOpportunityName || opportunity.opportunityName || '').trim();

          // utility methods
          opportunity.allowSubmissions = function() {
            var allowSubmission, opens, closes;
            if (this.opportunityType === 'Terminating') {
              opens = moment(this.submissionBeginDate).startOf('day');
              closes = moment(this.submissionEndDate).add(this.rules.gracePeriod, 'days').endOf('day');
              allowSubmission = opens.isBefore() && closes.isAfter();
            } else {
              allowSubmission = true;
            }
            return allowSubmission;
          };
          return opportunity;
        });
      }).service('opportunities'),

      Requests: XrasRestangular.withConfig(function(config) {
        config.setRestangularFields({'id': 'requestId'});

        config.addElementTransformer('requests', false, function(request) {
          // ensure values
          request.rules = request.rules || {};

          // are reviews visible to user
          request.isReviewsVisible = function() {
            return _.find(this.rules.existingActions, {reviewsViewable: true}) !== undefined;
          };

          // get reviews
          request.addRestangularMethod('reviews', 'get', 'reviews');

          return request;
        });
      }).service('requests'),

      Actions: {
        save: function(request, action) {

          if (action.actionId) {
            return request.customPUT(action, 'actions/' + action.actionId);
          } else {
            return request.customPOST(action, 'actions');
          }
        },
        validate: function(request, action) {
          var validate = request.one('actions', action.actionId).one('validate');

          // force refresh
          var cache = $cacheFactory.get('xrasApi');
          cache.remove(XrasRestangular.configuration.baseUrl + validate.getRestangularUrl());

          return validate.get();
        },
        submit: function(request, action) {
          var submission = request.one('actions', action.actionId).one('submit');
          return submission.save();
        },
        withdraw: function(request, action) {
          var submission = request.one('actions', action.actionId).one('submit');
          return submission.remove();
        },
        remove: function(request, action) {
          return request.one('actions', action.actionId).remove();
        }
      },

      Documents: {
        uploadUrl: function(request, action) {
          return request.one('actions', action.actionId).one('documents').getRestangularUrl();
        },
        downloadUrl: function(request, action, document) {
          return request.one('actions', action.actionId).one('documents', document.documentId).getRestangularUrl();
        },
        remove: function(request, action, document) {
          return request.one('actions', action.actionId).one('documents', document.documentId).remove();
        }
      },

      Grants: {
        save: function(request, grant) {
          if (grant.grantId) {
            return request.customPUT(grant, 'grants/' + grant.grantId);
          } else {
            return request.customPOST(grant, 'grants');
          }
        },
        remove: function(request, grant) {
          return request.one('grants', grant.grantId).remove();
        }
      },

      Resources: XrasRestangular.withConfig(function(config) {
        config.setRestangularFields({'id': 'resourceId'});
      }).service('resources'),

      People: XrasRestangular.withConfig(function(config) {
        config.setRestangularFields({'id': 'username'});
      }).service('people'),

      FundingAgencies: XrasRestangular.service('funding_agencies'),

      Fos: XrasRestangular.service('types/fos'),

      Roles: XrasRestangular.service('types/roles'),

      Units: XrasRestangular.service('types/units'),

      AttributeTypes: XrasRestangular.service('types/attribute_set_relations'),

      DocTypes: XrasRestangular.service('types/documents'),

      Users: XrasRestangular.service('users'),

      Publications: XrasRestangular.service('publications')
    };
  })
  .factory('RequestUtil', function(_) {
    return {
      /*
       * Returns a lexicographically sortable value for the role. Role names are
       * mapped to numeric ranks, then last name, then first name. For example,
       * the value for 'John Smith, PI' would be '0SmithJohn'.
       */
      requestRoleSort: function(role) {
        var rank;
        switch (role.roles[0].role) {
        case 'PI':
          rank = '0';
          break;
        case 'CoPI':
          rank = '1';
          break;
        case 'Allocation Manager':
          rank = '2';
          break;
        case 'NSF Fellow':
          rank = '3';
          break;
        case 'User':
          rank = '4';
          break;
        default:
          rank = '9';
        }
        return rank + role.person.lastName + role.person.firstName;
      },

      getOriginalAction: function(request) {
        return _.find(request.actions, function(action) {
          return action.actionType === 'New' || action.actionType === 'Renewal';
        });
      },

      requestStatusSort: function(status) {
        return _.indexOf(['Incomplete','Submitted','Under Review','Approved','Rejected'], status);
      },

      isAllowedOperation: function(request, op) {
        return _.contains(request.rules.allowedOperations, op);
      }
    };
  })
  .factory('OpportunityUtil', function() {
    return {
      defaultSort: function(opportunity) {
        var endDate;
        if (opportunity.opportunityType === 'Terminating') {
          endDate = opportunity.submissionEndDate;
        } else {
          endDate = '9999-99-99';
        }
        return opportunity.relativeOrder; //endDate + opportunity.name;
      },
      allocationTypeSummary: function(opportunity) {
        var summaries = {
          'Research': 'Research allocations may be requested for any compute, visualization, or storage resource and require a formal request document and CVs (for PI/Co-PIs). Research allocations are typically appropriate as follow-ons to Startups; but a PI need not request a Startup prior to submitting a Research request. A PI may submit a Research allocation request at any time during an active Startup allocation, if the PI has fulfilled his or her startup needs. A successful Research allocation will supersede any Startup allocation and start a new 12-month allocation period.',
          'Startup': 'The fastest way to get started on XSEDE, Startup allocations require minimum documentation, are reviewed all year long, and are valid for one year. Startup allocations vary in allotment size, depending on the resources requested. The total Startup allocation across all resources cannot exceed 200,000 SUs. For resource specific Startup limits, please see: <a href="https://portal.xsede.org/allocations-overview#sulimits" target="blank">XSEDE Startup limits table</a>.',
          'Campus Champions': 'The Campus Champions program supports campus representatives as a local source of knowledge about high-performance and high-throughput computing and other digital services, opportunities and resources.',
          'Educational': 'Education allocations are for academic or training classes such as those provided at a university that have specific begin and end dates. In addition to training classes, education projects support classroom instruction. Education allocations are appropriate for academic courses or training classes having a registration number, class description, and known timeframe. Education requests have the same allocation size limits as Startup requests. PIs on education allocations must be faculty.',
          'Software Testbeds': 'Testbeds are environments that allow users to conduct scientific experiments and typically consist of a variety of hardware and software components.'
        };
        return summaries[opportunity.allocationType];
      }
    };
  })
  .factory('AppUtil', function() {
    var service = {};

    service.convertBytes = function(bytes) {
      if (bytes < 1000) {
        return bytes + ' bytes';
      }
      var units = ['kB','MB','GB','TB','PB','EB','ZB','YB'];
      var u = -1;
      do {
        bytes = bytes / 1000;
        u = u+1;
      } while (bytes >= 1000);
      return bytes.toFixed(1) + ' ' + units[u];
    };

    service.uuid = function() {
      /*jshint bitwise: false*/
      return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random()*16|0, v = c === 'x' ? r : (r&0x3|0x8);
        return v.toString(16);
      });
      /*jshint bitwise: true*/
    };

    return service;
  })
  .factory('ResourceAttributeUtil', function(_) {
    return {
      map: function(action, resources) {
        var sorted = [];
        var map = {};
        var resourceArray = _.values(resources);
        var i, j, k;
        for (i = 0; i < resourceArray.length; i++) {
          var res = resourceArray[i];
          for (j = 0; j < res.attributeSets.length; j++) {
            var set = res.attributeSets[j];
            for (k = 0; k < set.attributes.length; k++) {
              var attr = set.attributes[k];
              var val = _.findWhere(action.resourceAttributes, {'resourceId': res.resourceId, 'attributeId': attr.attributeId});
              val = val || {
                resourceId: res.resourceId,
                attributeId: attr.attributeId,
                resourceAttributeId: attr.attributeId,
                attributeValue: ''
              };
              sorted.push(val);
              map[res.resourceId] = map[res.resourceId] || {};
              map[res.resourceId][attr.attributeId] = val;
            }
          }
        }
        action.resourceAttributes = sorted;
        return map;
      }
    };
  })
  .factory('OpportunityAttributeUtil', function(_) {
    return {
      map: function(action, opportunity) {
        var index = _.indexBy(action.opportunityAttributes, 'attributeId');
        var sorted = [];
        var map = {};
        var i, j;
        for (i = 0; i < opportunity.attributeSets.length; i++) {
          var set = opportunity.attributeSets[i];
          for (j = 0; j < set.attributes.length; j++) {
            var attr = set.attributes[j];
            var val = index[attr.attributeId] || {
              attributeId: attr.attributeId,
              opportunityAttributeId: attr.attributeId,
              attributeValue: ''
            };
            sorted.push(val);
            map[attr.attributeId] = val;
          }
        }
        action.opportunityAttributes = sorted;
        return map;
      }
    };
  })
  .factory('ResourceUtil', function() {
    var service = {};

    service.parseResourceName = function parseResourceName(resource) {
      var orig, match, parsed;
      orig = resource.displayResourceName || resource.resourceName;
      match = resource.displayResourceName.match(/(.+) \((.+)\)/);
      parsed = {};
      if (match) {
        parsed.name = match[2];
        parsed.description = match[1];
      } else {
        parsed.name = orig;
        parsed.description = '';
      }
      return parsed;
    };

    service.normalizedResourceType = function normalizedResourceType(resourceType) {
      switch (resourceType) {
      case 'Compute':
      case 'Machine':
      case 'Grid':
        resourceType = 'Compute';
        break;
      case 'Storage':
        resourceType = 'Storage';
        break;
      case 'Visualization':
        resourceType = 'Visualization';
        break;
      case 'Program':
        resourceType = 'Advanced Services';
        break;
      default:
        resourceType = 'Other';
      }
      return resourceType;
    };

    service.sortResourceType = function sortResourceType(resourceType) {
      var sort;
      switch (resourceType) {
      case 'Advanced Services':
        sort = 0;
        break;
      case 'Compute':
      case 'Machine':
      case 'Grid':
        sort = 1;
        break;
      case 'Storage':
        sort = 2;
        break;
      case 'Visualization':
        sort = 3;
        break;
      default:
        sort = 4;
      }
      return sort;
    };

    service.resourceTypeIcon = function resourceTypeIcon(resourceType) {
      var icon;
      switch (resourceType) {
      case 'Advanced Services':
        icon = 'other';
        break;
      case 'Compute':
      case 'Machine':
      case 'Grid':
        icon = 'gears';
        break;
      case 'Storage':
        icon = 'archive';
        break;
      case 'Visualization':
        icon = 'picture';
        break;
      default:
        icon = 'other';
      }
      return icon;
    };

    return service;
  });
