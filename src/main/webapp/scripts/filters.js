'use strict';
angular.module('xrasApp')

/**
 * Turns underscored phrases to human phrases. For example,
 * `this_is_a_name` becomes `This is a name`.
 */
.filter('unUnderscore', function() {
  return function(input) {
    return input.charAt(0).toUpperCase() + input.substr(1).replace(/[_]/g, ' ');
  };
});
