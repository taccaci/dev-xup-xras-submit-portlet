'use strict';
angular.module('xrasApp')
  .controller('MessagesCtrl', function ($scope, Notify) {
    $scope.messages = Notify.messages();
    $scope.$on('xras:notify', function() {
      $scope.messages = Notify.messages();
    });

    $scope.close = Notify.remove;
  });
