'use strict';
angular.module('xrasApp').controller('RequestFormResourcesCtrl',
  function($scope, $q, $filter, _, Api, RequestUtil, ResourceUtil, ResourceAttributeUtil) {

    $scope.$on('xras:requestReady', function() {

      var opportunityResourceIndex,
        availableResources,
        removedResources,
        processSelection,
        resourceAttributesForm;

      availableResources = _.chain($scope.opportunity.resources)
        .filter(function(resource) {
          return _.contains($scope.opportunity.rules.resourceIdsAvailableForNewRequest, resource.resourceId);
        })
        .each(function(resource) {
          _.extend(resource, ResourceUtil.parseResourceName(resource));
          resource._resourceType = resource.resourceType;
          resource.resourceType = ResourceUtil.normalizedResourceType(resource.resourceType);
        })
        .value();

      $scope.resourcesByType = _.groupBy(availableResources, 'resourceType');

      $scope.resourceTypes = _.keys($scope.resourcesByType);

      // silly ordering; put 'Other' last
      if (_.contains($scope.resourceTypes, 'Other')) {
        $scope.resourceTypes = _.without($scope.resourceTypes, 'Other');
        $scope.resourceTypes.push('Other');
      }

      $scope.opportunityResourceIndex = opportunityResourceIndex = _.indexBy(availableResources, 'resourceId');
      $scope.selectedResources = {};

      removedResources = {};
      $scope.resourceAttributeMap = {};

      resourceAttributesForm = function() {
        $scope.resourceAttributeMap = ResourceAttributeUtil.map($scope.action, $scope.selectedResources);
      };

      processSelection = function(resource, select) {
        var deferred = $q.defer(), i;
        if (select) {
          // check if already selected
          i = _.map($scope.action.resources, function(r) { return r.resourceId; }).indexOf(resource.resourceId);
          if (i === -1) {
            // add resource
            $scope.action.resources.push(removedResources[resource.resourceId] || { resourceId: resource.resourceId });
          }

          if ($scope.selectedResources[resource.resourceId].resourceId) {
            deferred.resolve($scope.selectedResources[resource.resourceId]);
          } else {
            // load rest of resource information
            Api.Resources.one(resource.resourceId).get().then(function(resp) {
              _.extend($scope.selectedResources[resource.resourceId], resp);
              deferred.resolve($scope.selectedResources[resource.resourceId]);
            });
          }
        } else {
          // un-select resource
          i = _.map($scope.action.resources, function(r) { return r.resourceId; }).indexOf(resource.resourceId);
          if (i > -1) {
            // cache entered values
            removedResources[resource.resourceId] = $scope.action.resources.splice(i, 1)[0];
          }

          deferred.resolve($scope.selectedResources[resource.resourceId]);
        }
        return deferred.promise;
      };

      $scope.dependenciesMet = function(resource) {
        var met = opportunityResourceIndex[resource.resourceId].requiredResources.length === 0;
        if (! met) {
          _.each(opportunityResourceIndex[resource.resourceId].requiredResources, function(r) {
            met = met || $scope.selectedResources[r.resourceId] && $scope.selectedResources[r.resourceId].selected;
          });
        }
        return met;
      };

      $scope.selectResource = function(resource) {
        // safety first!
        $scope.selectedResources[resource.resourceId] = $scope.selectedResources[resource.resourceId] || {};
        $scope.selectedResources[resource.resourceId].selected = ! $scope.selectedResources[resource.resourceId].selected;
        processSelection(resource, $scope.selectedResources[resource.resourceId].selected).then(resourceAttributesForm);
        $scope.$emit('xras:requestChanged', { key: 'resources' });
      };

      // get $scope.action currently selected resources;
      $q.all(_.map($scope.action.resources, function(r) {
        $scope.selectedResources[r.resourceId] = { selected: true };
        return processSelection(r, true);
      })).then(resourceAttributesForm);
    });

    $scope.resourceTypeIcon = ResourceUtil.resourceTypeIcon;

    $scope.sortResourceType = ResourceUtil.sortResourceType;

    $scope.sortSelectedResources = function(r) {
      if ($scope.opportunityResourceIndex && $scope.opportunityResourceIndex[r.resourceId]) {
        var oppResource = $scope.opportunityResourceIndex[r.resourceId];
        return ResourceUtil.sortResourceType(oppResource.resourceType) + '_' + oppResource.name;
      }
    };
  }
);
