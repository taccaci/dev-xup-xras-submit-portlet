'use strict';
angular.module('xrasApp').controller('RequestFormInfoCtrl', function($scope, _) {

  $scope.$on('xras:requestReady', function() {
    if (! $scope.opportunity.defaultAllocationAwardPeriod) {
      if ($scope.action.allocationDates.length === 0) {
        $scope.action.allocationDates[0] = {
          allocationDateType: 'Requested',
          beginDate: '',
          endDate: ''
        };
      }
      $scope.allocationDate = $scope.action.allocationDates[0];
    }
  });

  $scope.datepickerOpts = {
    format: 'yyyy-mm-dd',
    autoclose: true,
    startDate: 'today',
    placeholder: 'YYYY-MM-DD'
  };

  $scope.addFos = function() {
    $scope.request.fos = $scope.request.fos || []; // first fos!
    var fos = {};
    if ($scope.request.fos.length === 0) {
      fos.isPrimary = true;
    }
    $scope.request.fos.push(fos);
  };

  $scope.selectPrimaryFos = function(index) {
    _.each($scope.request.fos, function(field, i) {
      field.isPrimary = i === index;
    });
  };

  $scope.removeFos = function(index) {
    $scope.request.fos.splice(index, 1);
    $scope.$emit('xras:requestChanged', { key: 'fos' });
  };
});
