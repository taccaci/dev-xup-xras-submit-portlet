'use strict';
angular.module('xrasApp').controller('RequestReviewsCtrl', function(_, $scope, $routeParams, $location, Notify, Api) {
  $scope.loading = true;

  // fetch the request
  Api.Requests.one($routeParams.requestId).get().then(function(resp) {
    $scope.request = resp;
    $scope.actions = _.indexBy($scope.request.actions, 'actionId');

    // fetch the opportunity
    Api.Opportunities.one(resp.opportunityId).get().then(function(opp) {
      if (opp.adminComments) {
        var words = opp.adminComments.split(/\b/);
        if (words.length > 300) {
          opp.adminCommentsTruncated = true;
          opp._adminComments = opp.adminComments;
          words.length = 300;
          opp.adminComments = words.join('');
          if (! /\.$/.test(opp.adminComments)) {
            opp.adminComments = opp.adminComments + '...';
          }
        }
      }
      $scope.opportunity = opp;
    });

    // fetch the reviews
    resp.reviews().then(function(reviews) {
      $scope.reviews = _.indexBy(reviews.actions, 'actionId');
      var firstActionId = _.keys($scope.reviews)[0];

      if ($scope.reviews[firstActionId].reviews.length > 0) {
        // show first by default
        $scope.reviews[firstActionId].reviews[0].active = true;
      }

      // fetch resource details o_0
      $scope.resources = {};
      var resourceIds = _.chain(reviews.actions).map(function(action) {
        // dig into reviews object and get the resources on each action's review
        return _.chain(action.reviews).pluck('resources').value();
      })
      // flatten, pluck out the ids, and then unique it
      .flatten().pluck('resourceId').uniq().value();

      // do the resource lookups
      _.each(resourceIds, function(resourceId) {
        $scope.resources[resourceId] = Api.Resources.one(resourceId).get().$object;
      });

      $scope.loading = false;
    },
    function () {
      Notify.message({
        body: 'Unable to load the reviews for this Request! If this problem persists please contact the Help Desk.',
        type: 'danger'
      });
    });
  },
  function(error) {
    if (error.status === 404) {
      Notify.message({
        body: 'The Request was not found!',
        type: 'danger'
      });
    } else {
      Notify.message({
        body: 'There was an error loading the Request. Please try again.',
        type: 'danger'
      });
    }
    $location.path('/');
  });

  $scope.showMeetingComments = function() {
    $scope.opportunity.adminComments = $scope.opportunity._adminComments;
    $scope.opportunity.adminCommentsTruncated = false;
  };

  $scope.showReview = function(actionId, reviewId) {
    _.each($scope.reviews[actionId].reviews, function(review) {
      review.active = review.reviewId === reviewId;
    });
  };
});
