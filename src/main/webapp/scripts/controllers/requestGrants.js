'use strict';
angular.module('xrasApp').controller('RequestFormGrantsCtrl', function($scope, $timeout, $, _, Api, Notify) {

  $scope.$on('xras:requestReady', function() {
    // Get list of Funding Agencies
    Api.FundingAgencies.getList().then(function(list) {
      $scope.fundingAgencyList = list;
      $scope.fundingAgencyMap = _.indexBy(list, 'fundingAgencyId');
    });
  });

  $scope.editing = null;

  $scope.datepickerOpts = {
    format: 'yyyy-mm-dd',
    autoclose: true,
    placeholder: 'YYYY-MM-DD'
  };

  // add grant
  $scope.add = function() {
    $scope.request.grants = $scope.request.grants || [];
    $scope.edit($scope.request.grants.length, {awardedUnits: 'Dollars'});
  };

  $scope.edit = function($index, grant) {
    $scope.editing = angular.copy(grant);
    $scope.editing.$index = $index;
    $timeout(function() {
      $('html,body').scrollTop($('.block-ui-modal-dialog:visible').offset().top - 50);
    });
  };

  $scope.submit = function() {
    Notify.clear('grants');
    $scope.saveProgress = true;
    Api.Grants.save($scope.request, $scope.editing).then(
      function(resp) {
        _.extend($scope.request.grants, resp.grants);
        $scope.editing = null;
        $scope.saveProgress = false;

        Notify.message({
          body: 'Your Grant information was saved and added to the Request.',
          type: 'success'
        });
      },
      function(error) {
        Notify.message({
          key: 'grants',
          body: error.data,
          type: 'danger'
        });
        $scope.saveProgress = false;
      }
    );
  };

  $scope.cancel = function() {
    Notify.clear('grants');
    $scope.editing = null;
  };

  // remove grant
  $scope.remove = function($index, grant) {
    if (window.confirm('Are you sure you want to the grant "' + grant.title +'"?')) {
      Api.Grants.remove($scope.request, grant).then(
        function(/*resp*/) {
          Notify.message({
            body: 'This Grant has been deleted',
            type: 'success'
          });
          $scope.request.grants.splice($index, 1);
          // $scope.$emit('xras:requestChanged', { key: 'grants' });
        },
        function(error) {
          Notify.message({
            body: 'There was an error deleting this Grant. ' + error + ' If this problem persists, please contact the Help Desk.',
            type: 'danger'
          });
        }
      );
    }
  };

  $scope.close = Notify.remove;

  $scope.$on('xras:notify', function() {
    $scope.messages = Notify.messages();
  });
});
