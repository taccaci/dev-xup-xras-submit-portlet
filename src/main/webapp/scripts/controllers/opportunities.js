'use strict';
angular.module('xrasApp').controller('OpportunityCtrl', function ($scope, $routeParams, $location, Api, Notify, _) {
  Api.Opportunities.one($routeParams.opportunityId).get().then(function(opp) {
    $scope.opportunity = opp;
    $scope.opportunity.resources = _.filter(opp.resources, function(resource) {
      return _.contains(opp.rules.resourceIdsAvailableForNewRequest, resource.resourceId);
    });
  }, function(error) {
    if (error.status === 404) {
      Notify.message({
        body: 'The Opportunity was not found!',
        type: 'danger'
      });
    } else {
      Notify.message({
        body: 'There was and error loading the Opportunity. Please try again.',
        type: 'danger'
      });
    }
    $location.path('/');
  });

  $scope.showResourceDetails = {};
  $scope.toggleResourceDetails = function(resourceId) {
    $scope.showResourceDetails[resourceId] = ! $scope.showResourceDetails[resourceId];
  };
});
