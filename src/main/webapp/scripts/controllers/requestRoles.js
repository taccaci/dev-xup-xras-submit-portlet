'use strict';
angular.module('xrasApp').controller('RequestFormRoleCtrl', function($scope, _, Api) {

  $scope.$on('xras:requestReady', function() {
    // Roles
    Api.Roles.getList().then(function(list) {
      $scope.roles = list;
      var rejectRoles = ['User', 'NSF Fellow'];
      $scope.availableRoles = _.reject($scope.roles, function(role) {
        return _.contains(rejectRoles, role.roleType);
      });
    });
  });

  $scope.removeRole = function(role) {
    var index = _.indexOf($scope.request.roles, role);
    if (role.roleId) {
      if (window.confirm('Are you sure you want to remove this Person from this request?')) {
        $scope.request.roles.splice(index, 1);
        $scope.$emit('xras:requestChanged', { key: 'roles' });
      }
    } else {
      $scope.request.roles.splice(index, 1);
      $scope.$emit('xras:requestChanged', { key: 'roles' });
    }
  };

  $scope.isNotUser = function(role) {
    return ! _.chain(role.roles).pluck('role').contains('User').value();
  };

  $scope.search = {};

  $scope.singlePiRole = function(personRole) {
    return function(role) {
      if (! _.findWhere(personRole.roles, {role: 'PI'})) {
        if (role.roleType === 'PI') {
          return ! _.find($scope.request.roles, function(personRole) {
            return _.findWhere(personRole.roles, { role: 'PI' });
          });
        }
        return true;
      }
      return true;
    };
  };

  $scope.addRole = function() {

    if (! $scope.search.username) {
      return;
    }

    $scope.search.username = $scope.search.username.toLowerCase();

    $scope.loading = true;
    Api.People.one($scope.search.username).get().then(
      function(person) {
        $scope.loading = false;
        $scope.request.roles = $scope.request.roles || []; // is first role!
        if (person.username) {
            var alreadyAdded = _.find($scope.request.roles, function(role) { return role.person.username === person.username; });
            if (! alreadyAdded) {
              var role = {
                roles: [{role: ''}],
                person: person
              };
              $scope.request.roles.push(role);
              $scope.$emit('xras:requestChanged', { key: 'roles' });
              $scope.search.username = null;
            } else {
              window.alert('This person has already been added to the request as ' + _.pluck(alreadyAdded.roles, 'role').join(', ') + '!');
            }
        } else {
            window.alert('The user is unable to be added to the project at this time. Please make sure the user has successfully logged in to the XSEDE User Portal and accepted the Acceptable Use Policy. For additional help contact the XSEDE Help Desk.');
        }
      },
      function(error) {
        $scope.loading = false;
        if (error.status === 500) {
          window.alert('There was an error looking up user information! Please confirm that your have typed their username correctly and try again.');
        } else {
          window.alert('Unable to find a user with this username! Please check the username and try again.');
        }
      }
    );
  };

  $scope.userPrimaryRole = function(role) {
    var primaryRole;
    if (role.roles.length > 1) {
      primaryRole = _.find(role.roles, function(r) {
        return _.contains(['PI', 'CoPI', 'Allocation Manager'], r.role);
      });
    } else {
      primaryRole = role.roles[0];
    }
    return primaryRole;
  };

  $scope.isNsfFellow = function(role) {
    return _.findWhere(role.roles, {role: 'NSF Fellow'}) !== undefined;
  };

  $scope.showNsfFellowsOption = function(role) {
    return _.find(role.roles, function(role) { return _.contains(['PI', 'CoPI'], role.role); });
  };

  $scope.toggleNsfFellows = function($event, role) {
    $scope.$emit('xras:requestChanged', { key: 'roles' });
    if ($event.target.checked) {
      role.roles.push({role: 'NSF Fellow'});
    } else {
      role.roles = _.reject(role.roles, function(r) { return r.role === 'NSF Fellow'; });
    }
  };
});
