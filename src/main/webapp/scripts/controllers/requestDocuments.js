'use strict';
angular.module('xrasApp').controller('RequestFormDocumentsCtrl', function($scope, $timeout, $q, $upload, $, _, AppUtil, Api, Notify) {

  $scope.$on('xras:requestReady', function() {
    Api.DocTypes.getList().then(function(list) {
      $scope.docTypes = list;
      $scope.docTypesIndex = _.indexBy(list, 'documentType');

      Api.Requests.one().one('requirements').get().then(function(requirements) {
        var allocationType, allocationTypeId, actionType, docreqs, coPis, pageLimits, displayLimit;

        allocationType = $scope.opportunity.allocationType.toLowerCase().replace(/\s/g, '_');
        allocationTypeId = $scope.opportunity.allocationTypeInfo;

        actionType = $scope.action.actionType.toLowerCase();
        docreqs = requirements[allocationType][actionType].documents || [];

        Api.Requests.one().one('documentPageLimits').get().then(function(allPageLimits) {
            pageLimits = allPageLimits.documents;

            displayLimit = _.chain(pageLimits)
            .filter(function(pl) {
                return pl.allocationTypeId === allocationTypeId;
            })
            .filter(function(pl) {
                return _.contains($scope.opportunity.allocationType, pl.allocationType) && _.contains($scope.action.actionType, pl.actionType);
            })
            .value();

            if (actionType === 'New' || actionType === 'Renewal') {
              coPis = _.filter($scope.request.roles, function(role) {
                return _.where(role.roles, {role: 'CoPI'}).length > 0;
              });
              if (coPis.length > 0) {
                docreqs.push('CV_CoPI');
              }
            }

            $scope.documentRequirements = _.chain($scope.docTypes)
              .filter(function(docType) {
                return _.contains(docreqs, docType.documentType);
              })
              .pluck('displayDocumentType')
              .map(function(a) {
                var limit = _.find(displayLimit, function(d) {
                    return _.contains(a, d.displayDocumentType);
                });
                    if (limit && limit.pageLimit > 0) {
                    return a + ' (Page Limit: ' + limit.pageLimit + ')';
                    } else {
                        return a;
                    }
                })
              .value();
         });
      });
    });
  });

  $scope.editing = null;

  $scope.downloadDocumentUrl = function(doc) {
    return Api.Documents.downloadUrl($scope.request, $scope.action, doc);
  };

  $scope.removeDocument = function($index, doc) {
    if (window.confirm('Are you sure you want to delete the document "' + doc.title +'"?')) {
      Api.Documents.remove($scope.request, $scope.action, doc).then(
        function() {
          $scope.action.documents.splice($index,1);
          Notify.message({
            body: 'The document has been removed from this request.',
            type: 'success'
          });
        },
        function() {
          Notify.message({
            body: 'There was an error deleting this document. If this problem persists, please contact the Help Desk.',
            type: 'danger'
          });
        }
      );
    }
  };

  $scope.addDocument = function() {
    $scope.action.documents = $scope.action.documents || [];
    $scope.editing = {
      $index: $scope.action.documents.length
    };
    $timeout(function() {
      $('html,body').scrollTop($('.block-ui-modal-dialog:visible').offset().top - 50);
    });
  };

  $scope.onFileSelect = function($files) {
    $scope.$files = $files;
  };

  $scope.uploadDocument = function() {
    Notify.clear('docs');

    if ($scope.$files && $scope.$files.length > 0) {
      $scope.uploadInProgress = true;
      $scope.upload = $upload.upload({
        url: Api.Documents.uploadUrl($scope.request, $scope.action),
        method: 'POST',
        data: $scope.editing,
        file: $scope.$files[0],
        fileFormDataName: 'upload'
      })
      .success(function(data) {
        $scope.uploadInProgress = false;
        $scope.action.documents = _.findWhere(data.actions, { actionId: $scope.action.actionId }).documents;
        $scope.editing = null;
        Notify.message({
          body: 'Your document was uploaded successfully!',
          type: 'success'
        });
      })
      .error(function(err) {
        $scope.uploadInProgress = false;
        Notify.message({
          key: 'docs',
          body: err,
          type: 'danger'
        });
      });
    } else {
      Notify.message({
        key: 'docs',
        body: 'You haven\'t selected a file to upload!',
        type: 'danger'
      });
    }
  };

  $scope.cancelUpload = function() {
    Notify.clear('docs');
    $scope.editing = null;
  };

  $scope.convertBytes = AppUtil.convertBytes;

  $scope.close = Notify.remove;

  $scope.$on('xras:notify', function() {
    $scope.messages = Notify.messages();
  });
});
