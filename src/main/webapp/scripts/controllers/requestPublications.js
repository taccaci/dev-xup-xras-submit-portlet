'use strict';
angular.module('xrasApp').controller('RequestFormPublicationsCtrl', function($scope, $cacheFactory, _, Notify, Api) {

  $scope.$on('xras:requestReady', function() {
    $scope.updateAvailablePubs();
  });

  $scope.updateAvailablePubs = function() {
    $scope.refreshing = true;
    $scope.availablePublications = [];
    var usernames = _.chain($scope.request.roles).pluck('person').pluck('username').value().join(',');

    // force refresh always
    $cacheFactory.get('xrasApi').remove('/delegate/xras/publications/'+usernames);

    Api.Publications.one().customGETLIST(usernames).then(function(list) {
      $scope.refreshing = false;
      $scope.availablePublications = list;
    });
  };

  $scope.toggleAvailablePubs = function(show) {
    $scope.editing = show;
  };

  $scope.filterAddedPubs = function(publication) {
    return ! _.find($scope.request.publications, function(rp) {
      return rp.publication.id === publication.id;
    });
  };

  $scope.add = function(index, pub) {
    $scope.request.publications = $scope.request.publications || [];
    $scope.request.publications.push({
      publication: pub
    });
    $scope.$emit('xras:requestChanged', { key: 'publications' });
  };

  $scope.remove = function($index, pub) {
    if (window.confirm('Are you sure you want to remove the publication "' + pub.publication.title +'"?')) {
      $scope.request.publications.splice($index, 1);
      $scope.$emit('xras:requestChanged', { key: 'publications' });
    }
  };
});
