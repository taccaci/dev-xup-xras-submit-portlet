'use strict';
angular.module('xrasApp').controller('StartRequestCtrl', function ($scope, $routeParams, $location, Api, OpportunityUtil) {
  var opportunityId = $routeParams.opportunityId,
    renewalNumber = $routeParams.renewalNumber;

  var promise = Api.Opportunities.one(opportunityId).get();
  promise.then(function() {
    $scope.summaryInfo = OpportunityUtil.allocationTypeSummary($scope.opportunity);
  });
  $scope.opportunity = promise.$object;

  $scope.renewalNumber = renewalNumber;

  $scope.createRequest = function(advanced) {
    var req = {
      opportunityId: opportunityId
    };
    if (renewalNumber) {
      req.requestType = 'Renewal';
      req.requestNumber = $scope.renewalNumber;
    } else {
      req.requestType = 'New';
    }
    Api.Requests.post(req).then(function(resp) {
      var formUrl = '/requests/' + resp.requestId + (advanced ? '/edit/advanced' : '/edit');
      $location.path(formUrl);
    }, function() {
      window.alert('There was an error creating your Request! Please try again.');
    });
  };
});
