'use strict';
angular.module('xrasApp').controller('RequestFormUserCtrl', function($scope, _, Api) {

  $scope.$on('xras:requestReady', function(e, data) {
    $scope.request = data;
  });

  $scope.search = {};

  $scope.isUser = function(role) {
    return _.chain(role.roles).pluck('role').contains('User').value();
  };

  $scope.removeUser = function(role) {
    var index = _.indexOf($scope.request.roles, role);
    if (role.roleId) {
      if (window.confirm('Are you sure you want to remove this Person as a User on this request?')) {
        $scope.request.roles.splice(index, 1);
        $scope.$emit('xras:requestChanged', { key: 'roles' });
      }
    } else {
      $scope.request.roles.splice(index, 1);
      $scope.$emit('xras:requestChanged', { key: 'roles' });
    }
  };

  $scope.addUser = function() {

    if (! $scope.search.username) {
      return;
    }

    $scope.loading = true;
    Api.People.one($scope.search.username).get().then(
      function(person) {
        $scope.loading = false;
        $scope.request.roles = $scope.request.roles || []; // is first role!
        if (person.username) {
            var alreadyAdded = _.find($scope.request.roles, function(role) { return role.person.username === person.username; });
            if (! alreadyAdded) {
              var role = {
                roles: [{ role: 'User' }],
                person: person
              };
              $scope.request.roles.push(role);
              $scope.$emit('xras:requestChanged', { key: 'roles' });
              $scope.search.username = null;
            } else {
              window.alert('This person has already been added to the request as ' + _.pluck(alreadyAdded.roles, 'role').join(', ') + '!');
            }
        } else {
            window.alert('The user is unable to be added to the project at this time. Please make sure the user has successfully logged in to the XSEDE User Portal and accepted the Acceptable Use Policy. For additional help contact the XSEDE Help Desk.');
        }
      },
      function(error) {
        $scope.loading = false;
        if (error.status === 500) {
          window.alert('There was an error looking up user information! Please try again.');
        } else {
          window.alert('Unable to find a user with this username! Please check the username and try again.');
        }
      }
    );
  };
});
