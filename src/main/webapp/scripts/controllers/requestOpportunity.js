'use strict';
angular.module('xrasApp').controller('RequestFormOpportunityCtrl', function($scope, _, RequestUtil, OpportunityAttributeUtil) {
  $scope.$on('xras:requestReady', function() {
    $scope.opportunityAttributes = OpportunityAttributeUtil.map($scope.action, $scope.opportunity);
  });
});
