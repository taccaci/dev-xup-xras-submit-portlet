'use strict';
angular.module('xrasApp')
  .config(function($routeProvider) {
    $routeProvider.when('/', {
      templateUrl: 'views/main.html',
      controller: 'MainCtrl',
      label: 'XRAS'
    })
    .when('/opportunities/:opportunityId', {
      templateUrl: 'views/opportunity.html',
      controller: 'OpportunityCtrl',
      label: 'Opportunity'
    })
    .when('/opportunities/:opportunityId/new-request', {
      templateUrl: 'views/startRequest.html',
      controller: 'StartRequestCtrl',
      label: 'New Request'
    })
    .when('/opportunities/:opportunityId/renewal/:renewalNumber', {
      templateUrl: 'views/startRequest.html',
      controller: 'StartRequestCtrl',
      label: 'Renewal Request'
    })
    .when('/requests/:requestId', {
      templateUrl: 'views/request.html',
      controller: 'RequestCtrl',
      label: 'Request'
    })
    .when('/requests/:requestId/reviews', {
      templateUrl: 'views/requestReviews.html',
      controller: 'RequestReviewsCtrl',
      label: 'Reviews'
    })
    .when('/requests/:requestId/edit', {
      templateUrl: 'views/requestForm/wizard.html',
      controller: 'RequestFormWizardCtrl',
      label: 'Edit'
    })
    .when('/requests/:requestId/edit/advanced', {
      templateUrl: 'views/requestForm/advanced.html',
      controller: 'RequestFormAdvancedCtrl',
      label: 'Advanced'
    })
    .when('/requests/:requestId/new-action/:actionType', {
      controller: 'RequestNewActionCtrl',
      templateUrl: 'views/requestNewAction.html',
      label: 'New Action'
    })
    .when('/requests/:requestId/actions/:actionId', {
      controller: 'RequestEditActionCtrl',
      templateUrl: 'views/requestEditAction.html',
      label: 'Edit Action'
    })
    .when('/login', {
      templateUrl: 'views/loginRequired.html',
      label: 'Login Required'
    })
    .otherwise({
      redirectTo: '/'
    });
  })
  .run(function($rootScope, $location, $anchorScroll) {
    $rootScope.$on('$locationChangeStart', function(e, newUrl) {
      if (Liferay.ThemeDisplay.isSignedIn()) {
        // if signed in, prevent showing login
        if ($location.path() === '/login') {
          $location.path('/').replace();
        }
      } else {
        // if not signed in always show login
        $rootScope.loginRedirect = encodeURIComponent(newUrl);
        $location.path('/login').replace();
      }
    });

    $rootScope.$on('$routeChangeSuccess', function() {
      $anchorScroll();
    });
  });
