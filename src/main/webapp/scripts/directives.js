'use strict';
angular.module('xrasApp')
.directive('twbsTooltip', function($) {
  // call the Bootstrap tooltip plugin on the element
  return function(scope, el, attrs) {
    $(el).tooltip({ placement: attrs.placement || 'top'});
  };
})
.directive('ngEnter', function () {
  return function (scope, element, attrs) {
    element.bind('keydown keypress', function (event) {
      if(event.which === 13) {
        scope.$apply(function (){
          scope.$eval(attrs.ngEnter);
        });
        event.preventDefault();
      }
    });
  };
})
.directive('xrasAttrSetForm', function($compile, Api, _) {
  var handler = function(scope) {
    Api.AttributeTypes.one().get().then(function(map) {
      var template = '',
          type = map[scope.attributeSet.attributeSetRelationTypeId];

      switch (type.attributeSetRelationType) {
      case 'text':
        template = 'views/attributeSets/textFieldForm.html';
        break;
      case 'textarea':
        template = 'views/attributeSets/textAreaForm.html';
        break;
      case 'multi_sel':
        template = 'views/attributeSets/multiSelectForm.html';
        break;
      case 'single_sel':
        template = 'views/attributeSets/selectForm.html';
        scope.handleSelect = function(attribute) {
          _.each(scope.values, function(val) {
            if (val.attributeId !== attribute.attributeId) {
              val.attributeValue = '';
            }
          });
        };
        break;
      }

      scope.template = template;
    });
  };

  return {
    restrict: 'A',
    replace: true,
    link: handler,
    scope: {
      attributeSet: '=',
      values: '='
    },
    template: '<div ng-include="template"></div>'
  };

})
.directive('xrasAttrSetDisplay', function($compile, Api, _) {

  var handler = function(scope) {

    Api.AttributeTypes.one().get().then(function(map) {
      var template = '',
        type = map[scope.attributeSet.attributeSetRelationTypeId];

      switch (type.attributeSetRelationType) {
      case 'text':
      case 'textarea':
        template = 'views/attributeSets/textDisplay.html';
        break;
      case 'multi_sel':
      case 'single_sel':
        var selectedIds = _.pluck(
          _.filter(scope.values, function(val) {
            return val.attributeValue;
          }),
          'attributeId');
        scope.selectedAttributes = _.pluck(
          _.filter(scope.attributeSet.attributes, function(attr) {
            return _.contains(selectedIds, attr.attributeId);
          }), 'attributeName')
          .join(', ');

        template = 'views/attributeSets/selectDisplay.html';
        break;
      }
      scope.template = template;
    });

  };

  return {
    restrict: 'EA',
    link: handler,
    scope: {
      attributeSet: '=',
      values: '='
    },
    template: '<div ng-include="template"></div>'
  };
})
.directive('xrasCitation', function() {
  return {
    restrict: 'A',
    scope: {
      publication: '='
    },
    templateUrl: 'views/publication.html'
  };
})
// @TODO figure out what this does and where it's used
.directive('xrasRequirements', function(Api, _) {
  var handler = function(scope) {
    var allocationType, actionType, requirements, docs;

    allocationType = scope.allocationType.toLowerCase().replace(/\s/g, '_');
    actionType = scope.actionType.toLowerCase();

    Api.Requests.one().one('requirements').get().then(function(resp) {
      requirements = resp[allocationType];
      scope.reqs = _.keys(requirements[actionType]);

      docs = requirements[actionType].documents || [];
      if (docs.length > 0) {
        Api.DocTypes.getList().then(function(list) {
          scope.docs = _.chain(list)
            .filter(function(docType) {
              return _.contains(docs, docType.documentType);
            })
            .pluck('displayDocumentType')
            .value();
        });
      }
    });
  };

  return {
    restrict: 'A',
    link: handler,
    scope: {
      allocationType: '=',
      actionType: '='
    },
    templateUrl: 'views/xrasRequirements.html'
  };
})
.directive('xrasPerson', function() {
  return {
    restrict: 'A',
    scope: {
      person: '='
    },
    templateUrl: 'views/xrasPerson.html'
  };
})
.directive('ngTwbsDatepicker', function($) {
  return {
    restrict: 'A',
    replace: true,
    scope: {
      ngOptions: '=',
      ngModel: '='
    },
    template: '<div class="input-group"><span class="input-group-addon"><i class="icon icon-calendar"></i></span><input class="form-control" type="text"></div>',
    link: function(scope, element) {
      scope.inputHasFocus = false;
      scope.inputPlaceholder = scope.ngOptions.placeholder || scope.ngOptions.format || $.fn.datepicker.defaults.format;
      element.find('input').attr('placeholder', scope.inputPlaceholder);
      element.datepicker(scope.ngOptions).on('changeDate', function(e) {
        var format, language;
        format = scope.ngOptions.format || $.fn.datepicker.defaults.format;
        language = scope.ngOptions.language || $.fn.datepicker.defaults.language;
        return scope.$apply(function() {
          scope.ngModel = $.fn.datepicker.DPGlobal.formatDate(e.date, format, language);
          return scope.ngModel;
        });
      });
      element.find('input').on('focus', function() {
        scope.inputHasFocus = true;
        return scope.inputHasFocus;
      }).on('blur', function() {
        scope.inputHasFocus = false;
        return scope.inputHasFocus;
      });
      return scope.$watch('ngModel', function(newValue) {
        if (!scope.inputHasFocus) {
          return element.datepicker('update', newValue);
        }
      });
    }
  };
})
.directive('alert', function() {
  return {
    restrict: 'EA',
    replace: true,
    scope: {
      m: '=message',
      close: '&'
    },
    template: '<div class="alert alert-{{m.type}}"><button type="button" class="close" ng-click="close({message: m});">&times;</button><i class="icon icon-{{m.icon}}"></i> {{m.body}}</div>'
  };
})
.directive('xrasResourceTag', function(ResourceUtil) {
  return {
    restrict: 'A',
    scope: {
      resource: '='
    },
    link: function(scope) {
      var parsed = ResourceUtil.parseResourceName(scope.resource);
      scope.name = parsed.name;
      scope.description = parsed.description;
    },
    template: '<h4>{{name}}</h4><span>{{description}}</span>'
  };
});
