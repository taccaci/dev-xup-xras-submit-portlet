<h3>XSEDE Resource Allocation System <small>XRAS</small></h3>

<!--[if lt IE 9]>
<div class="jumbotron">
  <h3>You are using an <strong>outdated</strong> browser!</h3>
  <p>
    Please check that you have <a href="http://windows.microsoft.com/en-us/internet-explorer/use-compatibility-view" target="_blank">Compatibility Mode</a>
    turned <b>OFF</b> or upgrade your browser to a newer version to
    ensure that this application will work properly.
  </p>
</div>
<![endif]-->

<noscript>
  <div class="jumbotron">
    <h3>JavaScript is disabled!</h3>
    <p>
      It looks like you have JavaScript disabled. Please enable JavaScript to
      use this application.
    </p>
  </div>
</noscript>

<div id="ng-app" ng-app="xrasApp">
  <div class="impersonation" ng-controller="ImpersonationCtrl" ng-include="'views/impersonation.html'"></div>
  <div ng-controller="BreadcrumbsCtrl" ng-include="'views/breadcrumbs.html'"></div>
  <div ng-controller="MessagesCtrl" ng-include="'views/messages.html'"></div>
  <div ng-view></div>
</div>
