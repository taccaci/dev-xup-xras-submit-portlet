'use strict';

module.exports = function(grunt) {

  require('load-grunt-tasks')(grunt);
  require('time-grunt')(grunt);

  grunt.initConfig({

    // Project settings
    yeoman: {
      // configurable paths
      app: 'src/main/webapp',
      dist: 'dist'
    },

    clean: {
      all: [
        '.tmp/*',
        'src/main/webapp/js/*',
        'src/main/webapp/styles/*'
      ]
    },

    // Add vendor prefixed styles
    autoprefixer: {
      options: {
        browsers: ['last 1 version']
      },
      dist: {
        files: [{
          expand: true,
          cwd: '.tmp/styles/',
          src: '{,*/}*.css',
          dest: '.tmp/styles/'
        }]
      }
    },

    compass: {
      options: {
        sassDir: 'src/main/webapp/sass',
        cssDir: '.tmp/styles',
        imagesDir: 'src/main/webapp/images',
        javascriptsDir: 'src/main/webapp/js',
        fontsDir: 'src/main/webapp/styles/fonts',
        importPath: 'bower_components',
        relativeAssets: true
      },
      dist: {},
      dev: {
        options: {
          debugInfo: true
        }
      }
    },

    cssmin: {
      dist: {
        files: {
          'src/main/webapp/styles/main.css': [
            'bower_components/bootstrap-datepicker/css/datepicker3.css',
            '.tmp/styles/main.css'
          ]
        }
      }
    },

    svgmin: {
      dist: {
        files: [{
          expand: true,
          cwd: 'src/main/webapp/images',
          src: '{,*/}*.svg',
          dest: 'src/main/webapp/images'
        }]
      }
    },

    // ngmin tries to make the code safe for minification automatically by
    // using the Angular long form for dependency injection. It doesn't work on
    // things like resolve or inject so those have to be done manually.
    ngmin: {
      dist: {
        files: [{
          expand: true,
          cwd: '.tmp/concat/scripts',
          src: '*.js',
          dest: '.tmp/scripts'
        }]
      }
    },

    ngtemplates: {
      app: {
        options: {
          module: 'xrasApp'
        },
        cwd: 'src/main/webapp',
        src: 'views/{,*/}**.html',
        dest: '.tmp/scripts/templates.js'
      }
    },

    // Make sure code styles are up to par and there are no obvious mistakes
    jshint: {
      options: {
        jshintrc: '.jshintrc',
        reporter: require('jshint-stylish')
      },
      all: [
        'Gruntfile.js',
        'src/main/webapp/scripts/{,*/}*.js'
      ],
      test: {
        options: {
          jshintrc: 'test/.jshintrc'
        },
        src: ['test/spec/{,*/}*.js']
      }
    },

    copy: {
      dist: {
        files: [{
          expand: true,
          dot: true,
          cwd: 'src/main/webapp',
          dest: 'src/main/webapp',
          src: [
            '*.{ico,png,txt}',
            '.htaccess',
            '*.html',
            'views/{,*/}*.html',
            'images/{,*/}*.{webp}',
            'fonts/*',
          ]
        }, {
          expand: true,
          cwd: '.tmp/images',
          dest: 'src/main/webapp/images',
          src: ['generated/*']
        }, {
          expand: true,
          cwd: 'bower_components/ng-file-upload',
          dest: 'src/main/webapp/js',
          src: ['FileAPI.min.js', 'FileAPI.flash.swf']
        }]
      }
    },

    // Run some tasks in parallel to speed up the build process
    concurrent: {
      dist: [
        'compass:dist',
        'svgmin'
      ]
    },

    concat: {
      vendor: {
        src: [
          'bower_components/lodash/dist/lodash.js',
          'bower_components/momentjs/moment.js',
          'bower_components/twbs-bootstrap-sass/assets/javascripts/bootstrap/dropdown.js',
          'bower_components/twbs-bootstrap-sass/assets/javascripts/bootstrap/tooltip.js',
          'bower_components/bootstrap-datepicker/js/bootstrap-datepicker.js',
          'bower_components/ng-file-upload/angular-file-upload-shim.js',
          'bower_components/angular/angular.js',
          'bower_components/angular-route/angular-route.js',
          'bower_components/angular-animate/angular-animate.js',
          'bower_components/angular-sanitize/angular-sanitize.js',
          'bower_components/ng-breadcrumbs/dist/ng-breadcrumbs.js',
          'bower_components/angular-underscore/angular-underscore.js',
          'bower_components/restangular/dist/restangular.js',
          'bower_components/ng-file-upload/angular-file-upload.js',
          'bower_components/angular-wizard/dist/angular-wizard.js'
        ],
        dest: '.tmp/scripts/vendor.js'
      },
      scripts: {
        src: [
          'src/main/webapp/scripts/*.js',
          'src/main/webapp/scripts/controllers/*.js'
        ],
        dest: '.tmp/concat/scripts/scripts.js'
      }
    },

    uglify: {
      dist: {
        files: {
          'src/main/webapp/js/vendor.js': ['.tmp/scripts/vendor.js'],
          'src/main/webapp/js/scripts.js': ['.tmp/scripts/scripts.js', '.tmp/scripts/templates.js'],
          'src/main/webapp/js/json2.js': ['bower_components/json2/json2.js']
        }
      }
    }

  });

  grunt.registerTask('build', [
    'clean',
    'concurrent:dist',
    'autoprefixer',
    'concat',
    'ngmin',
    'ngtemplates',
    'copy:dist',
    'cssmin',
    'uglify'
  ]);

  grunt.registerTask('default', [
    'newer:jshint',
    'build'
  ]);
};
