XRAS Submit Portlet
===================

## Requirements

The following requirements are needed to build and deploy this portlet:

- maven
- ruby 1.8.7+ (for compass/sass)
  - the easiest thing here is to install [RVM](http://rvm.io) and install a modern ruby for the portal user.
  - `\curl -sSL https://get.rvm.io | bash -s stable --ruby=1.9.3`
- rubygems
  - compass
  - sass
- node.js
  - again, the easiest thing here is to install node for the local user
  - extract the lastest binary to ~/.local and add ~/.local/bin to the path
- grunt-cli and bower as global npm packages
  - `npm install -g grunt-cli bower`

## Project Dependencies

- XRAS Submit
- XSEDE Portal Shared

## Building

The following keys are expected to be present in `~/xsede-build-env.properties`

```
#XRAS Configuation
xras.api.host=api.example.org
xras.api.ssl=true
xras.api.baseUrl=/xras-submit-api
xras.api.version=/v1
xras.header.allocationsProcess=<process name>
xras.header.context=submit
xras.header.apiKey=<your api key>
```

You will need the following once all requirements are met and
the project dependencies have been installed to the local repository (`mvn install`)
then build with:

```
mvn package
```

Deploy to Liferay with:

```
mvn liferay:deploy
```
## Release History

#### v0.9.0 - (2014-06-27) - Friendly User Testing

#### v0.9.2 - (2014-07-09) - Friendly User Testing

#### v0.9.3 - (2014-07-10) - Friendly User Testing

#### v1.0.0 - (2014-08-20) - Initial public release

#### v1.1.0 - (2014-09-03) - Maintenance release

Fixes:
- XRAS-105: Secondary actions not filtering resources amount types
- XRAS-109: Secondary actions not displaying resources attributes
- XRAS-112: Secondary actions not prompting for document uploads
- XRAS-126: Make working on Transfer actions more clear as to requirements

#### v1.1.1 - (2014-09-03) -

#### v1.1.3 - (2014-09-10) - Address issue with missing Renewal Request Numbers

#### v1.1.4 - (2014-09-11) - bug fixes, improvements

Fixes:
- XRAS-138: Regression bug introduced by XRAS-105
- XRAS-51: improve logic for the display of opportunity submission options

#### v1.1.5 - (2014-09-12) - improved opportunity views

Fixes:
- XRAS-85: Opportunity name duplicated
- XRAS-137: Available resources listed for upcoming Research opportunity

#### v1.1.6 - (2014-09-16) - adding admin comments for reviews display

Fixes:
- XRAS-140
- XRAS-143

## XRAS Testing Instance

- XRAS Hostname: xras-test.tacc.utexas.edu
- Build the war and drop it in /home/portal/deploy
- The script at /home/portal/run_liferay.sh that will start the liferay image
- View docker container: $ docker ps -a (should show container modest_fermi)
- Restart docker container $ docker restart modest_fermi
- access the tomcat container (such as removing deployed portlets)
docker exec -it modest_fermi bash
then navigate to /opt/liferay/tomcat-7.0.40/webapps

## TODOS

- Figure out where the required document lists are coming from
- Figure out whether the validation.json file in resources is actually being used at all

- Added page limits hardcoded json file: need to find a workaround, preferably programmatic/in the API